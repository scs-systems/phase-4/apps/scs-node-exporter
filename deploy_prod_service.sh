#!/bin/bash

SCSREL=1.01

if [ "$SCSREL" = "" ] ; then
   echo "FAILURE: Must set SCSREL"
   exit 1
fi

set -x # 
cat <<EOT | kubectl apply -n web --force -f -
apiVersion: apps/v1
kind: DaemonSet
metadata:
  name: scs-node-exporter
  namespace: web
  labels:
    app: scs-node-exporter
spec:
  selector:
    matchLabels:
      app: scs-node-exporter
  template:
    metadata:
      labels:
        app: scs-node-exporter
    spec:
      tolerations:
      - key: node-role.kubernetes.io/master
        effect: NoSchedule
      containers:
      - name: scs-node-exporter
        image: gitlab.scsuk.net:5005/scs-systems/ext_registry/prom/node-exporter:v0.18.1
        args:
        - "--path.procfs=/host/proc"
        - "--path.sysfs=/host/sys"
        - "--path.rootfs=/rootfs"
        ports:
        - containerPort: 9100
        volumeMounts:
        - name: proc
          readOnly:  true
          mountPath: /host/proc
        - name: sys
          readOnly: true
          mountPath: /host/sys
        - name: rootfs
          readOnly: true
          mountPath: /rootfs
      hostNetwork: true
      volumes:
      - name: proc
        hostPath:
          path: /proc
      - name: sys
        hostPath:
          path: /sys
      - name: rootfs
        hostPath:
          path: /
EOT

echo
kubectl -n web get ds scs-node-exporter
echo
kubectl -n web get pods -l app=scs-node-exporter -o wide
